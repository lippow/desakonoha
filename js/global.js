var app = {
  init: function() {
    app.homeSlider();
    //app.aosEffect();
    app.headerScroll();
    app.tesimonialSlider();
  },
  homeSlider: function(){
    var heroSlider = jQuery('#slider-main.owl-carousel');
    heroSlider.owlCarousel({
      loop:true,
      nav:true,
      autoplay:true,
      autoplayTimeout:5000,
      autoplayHoverPause:true,
      //animateOut: 'fadeOut',
      navText : ["<i class='ti-angle-left'></i>","<i class='ti-angle-right'></i>"],
      responsive:{
          0:{
              items:1
          },
          600:{
              items:1
          },
          1000:{
              items:1
          }
      }
    })

    // heroSlider.on("changed.owl.carousel", function(event){
    //     // selecting the current active item
    //     var item = event.item.index -2;
    //     // var item = event.item.index;
    //     // window.location.hash = item + 1;
    //     // first removing animation for all captions
    //     jQuery('.inner-text h2').removeClass('animated fadeInUp');
    //     jQuery('.inner-text p').removeClass('animated fadeInLeft');
    //     jQuery('.inner-button').removeClass('animated fadeInRight');
    //     jQuery('.owl-item').not('.cloned').eq(item).find('.inner-text h2').addClass('animated fadeInUp');
    //     jQuery('.owl-item').not('.cloned').eq(item).find('.inner-text p').addClass('animated fadeInLeft');
    //     jQuery('.owl-item').not('.cloned').eq(item).find('.inner-button').addClass('animated fadeInRight');
    // })



    // add animate.css class(es) to the elements to be animated
    function setAnimation ( _elem, _InOut ) {
      // Store all animationend event name in a string.
      // cf animate.css documentation
      var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

      _elem.each ( function () {
        var $elem = jQuery(this);
        var $animationType = 'animated ' + $elem.data( 'animation-' + _InOut );

        $elem.addClass($animationType).one(animationEndEvent, function () {
        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
        });
      });
    }

  // Fired before current slide change
    heroSlider.on('change.owl.carousel', function(event) {
        var $currentItem = jQuery('.owl-item', heroSlider).eq(event.item.index);
        var $elemsToanim = $currentItem.find("[data-animation-out]");
        setAnimation ($elemsToanim, 'out');
    });

  // Fired after current slide has been changed
    heroSlider.on('changed.owl.carousel', function(event) {

        var $currentItem = jQuery('.owl-item', heroSlider).eq(event.item.index);
        var $elemsToanim = $currentItem.find("[data-animation-in]");
        setAnimation ($elemsToanim, 'in');
    })
  },

  // aosEffect: function(){
  //   AOS.init({
  //     once: true
  //   });
  // }

  headerScroll: function(){
    $(document).ready(function() {
        $(document).scroll(function() {
            var y = $(this).scrollTop();
            if (y > 20) {
                $( "body .navbar" ).addClass('changeups');
            } else {
                $( "body .navbar").removeClass('changeups');
            }
        });
    });
},

tesimonialSlider: function(){
  jQuery('#slider-testimonial').owlCarousel({
    loop:true,
    nav:true,
    margin: 20,
    autoplay:true,
    autoplayTimeout:13000,
    autoplayHoverPause:true,
    navText : ["<i class='icon-left-open-big'></i>","<i class='icon-right-open-big'></i>"],
    responsive:{
        0:{
          items:1
        },
        768:{
          items:1
        },
        1025:{
          items:2
        }
    }
  })
},

};
jQuery(document).ready(function($){
app.init();
})
